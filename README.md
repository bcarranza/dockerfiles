# :whale:

Rather than stringing `find` and `history` together to find that `Dockerfile`, I had, I am going to start storing them here. 

  - `Dockerfile.sayhelloviahttp` -- An Alpine based image that uses [httpie](https://httpie.io/) to say hello to `https://www.google.com`. 
  - `whoami/Dockerfile` -- Learn about who you are, inside a Docker container. 
