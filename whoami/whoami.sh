#!/bin/bash

# who even am I? 
id -u
id -g
echo $USER
whoami

# checking directory ownership and perms
echo "Checking ownership and permissions..."
pwd
ls -ahls 
cd ..
ls -ahls 

# umask
echo "Asking after umask..."
umask


